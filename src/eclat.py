#!/usr/bin/env python3


"""Module for defining the Eclat data-mining algorithm"""


from it_node import ITNode
from file_reader import FileReader
from print_util import Printer


class Eclat():

    def __init__(self, min_supp, input_filename, names, tids):
        """
        Constructor method for the EclatClose class
        :param min_supp: integer, minimum support
        :param input_filename: string, input filename
        :param names: boolean, True if the itemsets are to represented with letters, False otherwise
        :param tids: boolean, True if the tidSets are to shown, False otherwise
        """
        self.min_supp = min_supp
        self.file_reader = FileReader(input_filename, names)
        self.frequent_itemsets = []
        self.tids=tids

    def extend(self, node):
        """
        Method for recursively extend the search tree of the algorithm

            :param node - the node to be extended
        """
        siblings = node.get_parent().get_children()
        siblings = siblings[1:]
        for sibling in siblings:
            candidate = self.get_candidate(node, sibling)
            if candidate is not None:
                node.add_child(candidate)

        while node.get_children():
            child = node.get_children()[0]
            self.extend(child)
            self.save(child)
            self.delete(child)

    def save(self, node):
        """
        Method for saving the given node
        In our case this means printing to the standard output

        :param node - the node to be saved
        """
        #print(node)
        self.frequent_itemsets.append(node)

    def delete(self, node):
        """
        Method for deleting a node

        :param node - the node to be deleted
        """
        if not node.get_children():
            node.get_parent().get_children().remove(node)

    def get_candidate(self, current, sibling):
        """
        Method for getting a possible candidate ITNode from the given sibling nodes

        :param current: the current node
        :param sibling: immediate sibling of current

        :return: new candidate if the new node's support is greater or equal than the min_supp, None if not
        """
        cand_itemset = current.get_itemset() | sibling.get_itemset()
        cand_tidset = current.get_tidset() & sibling.get_tidset()

        if len(cand_tidset) >= self.min_supp:
            candidate = ITNode(cand_itemset, cand_tidset, current, len(cand_tidset))
            return candidate
        else:
            return None

    def eclat(self):
        """
        Algorithm for calculating frequent itemsets using the ECLAT data-mining algorithm.
        """
        root = ITNode(set(), self.file_reader.get_first_tid(), None, self.min_supp)

        for attr in self.file_reader.get_attributes():
            if attr[2] >= self.min_supp:
                root.add_child(ITNode(attr[0], attr[1], root, attr[2]))

        self.file_reader = None

        while root.get_children():
            child = root.get_children()[0]
            self.extend(child)
            self.save(child)
            self.delete(child)

    def print(self):
        """
        Method for printing out the results of the algorithm using the print_util.Printer class
        :return: None
        """
        Printer(
            name="eclat",
            min_supp=self.min_supp,
            frequent_itemsets=self.frequent_itemsets,
            tids=self.tids
        ).print()

    def run(self):
        """
        Method for running the algorithm and printing its results to the standard output
        :return: None
        """
        self.eclat()
        self.print()