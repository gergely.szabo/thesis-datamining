from eclat import Eclat
from eclat_close import EclatClose
from charm import Charm

class DataMinerConfig:
    """
    Class for defining a config object for the DataMiner class
    """
    def __init__(self, algorithm, min_supp, input_filename, names=False, tids=False, closed=False):
        """
        Constructor method
        :param algorithm: string, name of the algorithm
        :param min_supp: integer, minimum support
        :param input_filename: string, input filename
        :param names: boolean, True if itemsets are to represented with letters, False otherwise
        :param tids: boolean, True if tidsets are to shown, False otherwise
        :param closed: boolean, True if only closed itemsets are to shown, False otherwise
        """
        self.algorithm = algorithm
        self.input_filename = input_filename
        self.min_supp = min_supp
        self.names = names
        self.closed = closed
        self.tids = tids

class DataMiner:
    """
    Class for running algorithms based on configuration
    """
    def __init__(self, config):
        self.config = config
        self.supported_algorithms = ["eclat", "eclat-close", "charm"]


    def run_algorithm(self):
        """
        Runs the selected algorithm with the given config
        :return: None
        """
        if self.config.algorithm in self.supported_algorithms:
            if self.config.algorithm == "eclat":
                Eclat(
                    min_supp=self.config.min_supp,
                    input_filename=self.config.input_filename,
                    names=self.config.names,
                    tids=self.config.tids
                ).run()

            elif self.config.algorithm == "eclat-close":
                    EclatClose(
                        min_supp=self.config.min_supp,
                        input_filename=self.config.input_filename,
                        names=self.config.names,
                        tids=self.config.tids,
                        closed=self.config.closed
                    ).run()

            elif self.config.algorithm == "charm":
                Charm(
                    min_supp=self.config.min_supp,
                    input_filename=self.config.input_filename,
                    names=self.config.names,
                    tids=self.config.tids,
                ).run()
        else:
            print("algorithm [" + self.config.algorithm + "] is not among the supported algorithms")