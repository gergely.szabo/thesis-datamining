from string_utils import StringUtils

class Printer:
    """
    Class for printing out non closed algorithm results to the standard output
    """
    def __init__(self, name, min_supp, frequent_itemsets, tids):
        """
        Constructor method
        :param name: string, name of the algorithm
        :param min_supp: integer, minimum support
        :param frequent_itemsets: array, array of frequent itemsets
        :param tids: boolean, True if tidsets are to shown, False otherwise
        """
        self.name = name
        self.min_supp = min_supp
        self.frequent_itemsets = frequent_itemsets
        self.tids = tids

    def print(self):
        """
        Method for printing the result
        :return: None
        """
        print('current algorithm: ' + self.name)
        print('minimum support: ' + str(self.min_supp))
        print('output format: {itemset}[ x {tidset}](support)')
        print('number of frequent itemsets: ' + str(len(self.frequent_itemsets)))
        print('frequent item sets:')
        if self.tids:
            for node in self.frequent_itemsets:
                print(str(node) + "(" + str(node.get_support()) + ")")
        else:
            for node in self.frequent_itemsets:
                itemset = ", ".join([str(x) for x in sorted(list(node.get_itemset()))])
                print("{" + itemset + "}" + "(" + str(node.get_support()) + ")")


class ClosedPrinter:
    """
    Class for printing out closed algorithm results to the standard output
    """
    def __init__(self, name, min_supp, frequent_itemsets, tids, closed_itemsets, closed):
        """
        Constructor method
        :param name: string, name of the algorithm
        :param min_supp: integer, minimum support
        :param frequent_itemsets: array, array of frequent itemsets
        :param tids: boolean, True if tidsets are to shown, False otherwise
        :param closed_itemsets: dictionary, dictionary of closed itemsets
        :param closed: boolean, True if only closed patterns are to shown, False otherwise
        """
        self.name = name
        self.min_supp = min_supp
        self.frequent_itemsets = frequent_itemsets
        self.tids = tids
        self.closed_itemsets = closed_itemsets
        self.closed = closed

    def print(self):
        """
        Method for printing the result
        :return: None
        """
        if not self.closed:
            print('current algorithm: ' + self.name)
            print('minimum support: ' + str(self.min_supp))
            print('output format: {itemset}[ x {tidset}](support)')
            print('number of frequent itemsets: ' + str(len(self.frequent_itemsets)))
            print('frequent item sets:')
            if self.tids:
                for node in self.frequent_itemsets:
                    print(str(node) + "(" + str(node.get_support()) + ")")
            else:
                for node in self.frequent_itemsets:
                    itemset = ", ".join([str(x) for x in sorted(list(node.get_itemset()))])
                    print("{" + itemset + "}" + "(" + str(node.get_support()) + ")")

            print('\nnumber of frequent closed itemsets: ' + str(len(self.closed_itemsets)))
            print("frequent closed patterns:")
            if self.tids:
                print(StringUtils.dict_to_string_with_tids(self.closed_itemsets))
            else:
                print(StringUtils.dict_to_string_without_tids(self.closed_itemsets))
        else:
            print('current algorithm: ' + self.name)
            print('minimum support: ' + str(self.min_supp))
            print('number of frequent closed itemsets: ' + str(len(self.closed_itemsets)))
            print("frequent closed patterns:")
            if self.tids:
                print(StringUtils.dict_to_string_with_tids(self.closed_itemsets))
            else:
                print(StringUtils.dict_to_string_without_tids(self.closed_itemsets))