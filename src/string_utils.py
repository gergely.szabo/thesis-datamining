#!/usr/bin/env python3


class StringUtils():
    """
    Class for defining methods to convert patterns into strings
    """
    @staticmethod
    def dict_to_string_with_tids(d):
        """
        Method used to convert closed pattern dictionaries to string with their tidsets included
        :param d: dictionary, dictionary with closed pattens
        :return: string
        """
        res = ""
        for key in d:
            res += "{"
            res += ", ".join([str(item) for item in d[key]])
            res += "} x {"
            res += ", ".join([str(item) for item in key])
            res += "}"
            res += "("
            res += str(len(key))
            res += ")\n"
        return res

    @staticmethod
    def dict_to_string_without_tids(d):
        """
        Method used to convert closed pattern dictionaries to string without their tidsets
        :param d: dictionary, dictionary with closed pattens
        :return: string
        """
        res = ""
        for key in d:
            res += "{"
            res += ", ".join([str(item) for item in d[key]])
            res += "}"
            res += "("
            res += str(len(key))
            res += ")\n"
        return res
