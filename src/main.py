#!/usr/bin/env python3


from dataminer import DataMinerConfig, DataMiner
import sys
import time


algorithms = ['eclat', 'eclat-close']


def usage():
    print("""Usage: ./main.py <input_filename> <minimum_support> -alg:<algorithm> [-names] [-tids] [-closed]
            input_filename:     input file with the dataset, must be .rcf format
            minimum_support:    minimum support for the algorithm
            algorithm:          algorithm to use
            
            -names:             itemsets will be represented with letters if present and with numbers otherwise
            -tids:              tidsets will be present if given, omitted otherwise
            -closed:            prints only the closed itemsets (will be omitted if the chosen algorithm does not calculate closed itemsets)
            """)


def main():
    if len(sys.argv) < 4:
        usage()
    else:
        if sys.argv[3][:5] == "-alg:":
            alg_name = sys.argv[3].split(":")[1]
            min_supp = int(sys.argv[2])
            input_filename = sys.argv[1]
            config = DataMinerConfig(algorithm=alg_name, min_supp=min_supp, input_filename=input_filename)

            if "-names" in sys.argv:
                config.names = True

            if "-tids" in sys.argv:
                config.tids = True

            if "-closed" in sys.argv:
                config.closed = True

            start = time.time()
            DataMiner(config).run_algorithm()
            end = time.time()
            print("Execution time: %.4f s" % (end - start))

        else:
            usage()

if __name__ == '__main__':
    main()
