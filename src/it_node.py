#!/usr/bin/env python3


"""Class for abstraction of IT-NODES used by the search tree of eclat"""


class ITNode():

    def __init__(self, itemset, tidset, parent, support):
        self.itemset = itemset
        self.tidset = tidset
        self.parent = parent
        self.support = support
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        return self.children

    def get_itemset(self):
        return self.itemset

    def get_tidset(self):
        return self.tidset

    def get_parent(self):
        return self.parent

    def get_support(self):
        return self.support

    def __str__(self):
        res = "{"
        temp = [str(x) for x in sorted(list(self.itemset))]
        res += ", ".join(temp)
        res += "} x {"
        temp = [str(x) for x in sorted(list(self.tidset))]
        res += ", ".join(temp)
        res += "}"
        return res

    def __repr__(self):
        return str(self)
