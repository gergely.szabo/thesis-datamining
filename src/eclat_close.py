#!/usr/bin/env python3


"""Module for defining the Eclat-Close data-mining algorithm"""


from it_node import ITNode
from file_reader import FileReader
from print_util import ClosedPrinter
from eclat import Eclat


class EclatClose(Eclat):

    def __init__(self, min_supp, input_filename, names, tids, closed):
        """
        Constructor method for the EclatClose class
        :param min_supp: integer, minimum support
        :param input_filename: string, input filename
        :param names: boolean, True if the itemsets are to represented with letters, False otherwise
        :param tids: boolean, True if the tidSets are to shown, False otherwise
        :param closed: boolean, True if only the closed patterns are to shown, False otherwise
        """
        super().__init__(min_supp, input_filename, names, tids)
        self.closed=closed
        self.closed_patterns = {}

    def save(self, node):
        """
        Method for processing the given node
        The algorithm computes the closed-patterns of the dataset here.

        :param node - node to process
        """
        if tuple(sorted(node.get_tidset())) in self.closed_patterns.keys():
            if len(node.get_itemset()) > len(self.closed_patterns[tuple(sorted(node.get_tidset()))]):
                self.closed_patterns[tuple(sorted(node.get_tidset()))] = sorted(node.get_itemset())
        else:
            self.closed_patterns[tuple(sorted(node.get_tidset()))] = sorted(node.get_itemset())

        self.frequent_itemsets.append(node)

    def print(self):
        """
        Method for printing out the results of the algorithm using the print_util.ClosedPrinter class
        :return: None
        """
        ClosedPrinter(
            name="eclat-close",
            min_supp=self.min_supp,
            frequent_itemsets=self.frequent_itemsets,
            tids=self.tids,
            closed_itemsets=self.closed_patterns,
            closed=self.closed
        ).print()

    def run(self):
        """
        Method for running the algorithm and printing its results to the standard output
        :return: None
        """
        self.eclat()
        self.print()