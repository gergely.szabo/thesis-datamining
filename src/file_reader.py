#!/usr/bin/env python3


"""Utility for reading and processing input files for the Eclat algorithm to use"""


class FileReader():

    def __init__(self, filename="", is_named=False):
        self.filename = filename
        self.is_named = is_named
        self.items = []
        self.columns = []
        self.name = ""
        self.data = []
        self.attributes = []
        self.read()

    def read(self):
        with open(self.filename, 'r') as f:
            for line in f:
                if line.strip() == "[Binary Relation]":
                    self.name = f.readline()
                    self.columns = f.readline().strip().split(" | ")
                    self.items = [item for item in f.readline().strip().split(" | ")]
                    while True:
                        line = f.readline().strip()
                        if line != "[END Relational Context]":
                            self.data.append(line.split())
                        else:
                            break

        if self.is_named:
            self.make_named_attributes()
        else:
            self.make_non_named_attributes()

        for i in range(len(self.items)):
            for j in range(len(self.data)):
                if self.data[j][i] == '1':
                    self.items[i][1].add(j+1)

        for item in self.items:
            self.attributes.append((item[0], item[1], len(item[1])))

    def get_name(self):
        return self.name

    def get_columns(self):
        return self.columns

    def get_items(self):
        return self.items

    def get_data(self):
        return self.data

    def get_attributes(self):
        return self.attributes

    def make_named_attributes(self):
        for i in range(len(self.items)):
            self.items[i] = ({self.items[i]}, set())

    def make_non_named_attributes(self):
        for numeral in range(len(self.items)):
            self.items[numeral] = ({numeral+1}, set())

    def get_first_tid(self):
        res = set()
        for i in range(len(self.columns)):
            res.add(i+1)
        return res
