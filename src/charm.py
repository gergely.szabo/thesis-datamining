#!/usr/bin/env python3


"""Module for defining the Eclat data-mining algorithm"""


from it_node import ITNode
from file_reader import FileReader
from print_util import Printer
from eclat import Eclat


class Charm(Eclat):

    def __init__(self, min_supp, input_filename, names, tids):
        super().__init__(min_supp, input_filename, names, tids)
        self.frequent_closed_itemsets = {}

    def replace(self, current_node, pattern):
        current_node.itemset = current_node.itemset | pattern
        for child in current_node.get_children():
            child.itemset = child.itemset | pattern
            self.replace(child, pattern)

    def save(self, node):

        k = str(sum(node.tidset))
        if k in self.frequent_closed_itemsets.keys():
            closed_patterns_at_key = self.frequent_closed_itemsets[k]
            for pattern in closed_patterns_at_key:
                if node.support == pattern.support:
                    if node.itemset <= pattern.itemset:
                        pass
                    else:
                        self.frequent_closed_itemsets[k].append(node)
        else:
            self.frequent_closed_itemsets[k] = []
            self.frequent_closed_itemsets[k].append(node)

    def extend(self, node):
        """
        Method for recursively extend the search tree of the algorithm

        :param node - the node to be extended
        """
        siblings = node.get_parent().get_children()
        siblings = siblings[1:]
        for sibling in siblings:
            if node.tidset == sibling.tidset:
                self.replace(node, sibling.itemset)
                self.delete(sibling)
            elif node.tidset <= sibling.tidset:
                self.replace(node, sibling.itemset)
            elif sibling.tidset <= node.tidset:
                candidate = self.get_candidate(node, sibling)
                self.delete(sibling)
                if candidate is not None:
                    node.add_child(candidate)
            else:
                candidate = self.get_candidate(node, sibling)
                if candidate is not None:
                    node.add_child(candidate)

        while node.get_children():
            child = node.get_children()[0]
            self.extend(child)
            self.save(child)
            self.delete(child)

    def print(self):
        """
        Method for printing out the results of the algorithm using the print_util.Printer class
        :return: None
        """
        Printer(
            name="charm",
            min_supp=self.min_supp,
            frequent_itemsets=self.frequent_itemsets,
            tids=self.tids
        ).print()

    def run(self):
        """
        Method for running the algorithm and printing its results to the standard output
        :return: None
        """
        self.eclat()
        for k in self.frequent_closed_itemsets:
            self.frequent_itemsets += self.frequent_closed_itemsets[k]
        self.print()