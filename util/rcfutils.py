#!/usr/bin/env python3


# idea :
# https://stackoverflow.com/questions/29351492/
# how-to-make-a-continuous-alphabetic-list-python-from-a-z-then-from-aa-ab-ac-e


from string import ascii_lowercase
import itertools
import random


def column_name_generator():
    """Generator function for generating the prefixes of the columns"""
    size = 1
    while True:
        for s in itertools.product(ascii_lowercase, repeat=size):
            yield "".join(s)
        size += 1


def generate_column_names(column_count):
    """
    Function for creating a list of prefixes for data set columns

        :param column_count: number of columns that need prefix

        :return: list of prefixes
    """

    result = [name for name in itertools.islice(column_name_generator(), column_count)]
    return result


def generate_row_names(row_count):
    """
    Function for creating a list of prefixes for data set rows

        :param row_count - number of rows that need prefix

        :return list of prefixes
    """

    result = ["o" + str(num + 1) for num in range(row_count)]
    return result

def generate_random_list(ones, zeros):
    result = [1 for x in range(ones)] + [0 for x in range(zeros)]
    random.shuffle(result)
    return result