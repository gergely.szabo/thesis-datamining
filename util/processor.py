#!/usr/bin/env python3

import rcfutils as rcf
import os
import sys


class DatasetProcessor:
    def __init__(self, filename):
        self.filename = filename
        self.rows = 0
        self.columns = 0
        self.result = []
        self.data = []

    def process_data(self):
        with open(self.filename, 'r') as f:
            for line in f:
                line_as_list = line.strip().split(' ')
                line_as_list = [int(item) for item in line_as_list]
                if max(line_as_list) > self.columns:
                    self.columns = max(line_as_list)
                self.result.append(line_as_list)
                self.rows += 1

    def generate_data(self):
        i = 0
        while i < self.rows:
            temp = [0 for item in range(self.columns)]
            for item in self.result[i]:
                temp[item - 1] = 1
            self.data.append(temp)
            i += 1

    def generate_output_file(self):
        self.process_data()
        self.generate_data()

        if not os.path.exists("generated_files"):
            os.makedirs("generated_files")
        filename = self.filename + ".rcf"
        path = "generated_files/" + filename
        row_names = ' | '.join(map(str, rcf.generate_row_names(self.rows)))
        column_names = ' | '.join(map(str, rcf.generate_column_names(self.columns)))

        with open(path, 'w') as output_file:
            output_file.write("[Relational Context]\n")
            output_file.write(filename + "\n")
            output_file.write("[Binary Relation]\n")
            output_file.write(filename.split('.')[0] + "\n")
            output_file.write(row_names + "\n")
            output_file.write(column_names + "\n")
            for row in self.data:
                output_file.write(' '.join(map(str, row)) + '\n')
            output_file.write("[END Relational Context]\n")


def main():
    try:
        input = sys.argv[1]
        DatasetProcessor(input).generate_output_file()
    except(IndexError):
        print("missing filename")


if __name__ == '__main__':
    main()

