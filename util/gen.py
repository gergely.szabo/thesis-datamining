#!/usr/bin/env python3


import rcfutils as rcf
import datetime
import os
import sys


def generate_data(rows, columns, percentage):
    """
    Function for generating a list of random numbers where the density of ones equals to given the percentage

        :param rows: the amount of rows in the data set
        :param columns: the amount of columns in the data set
        :param percentage: the percentage of ones in the list given in percent format

        :return: a list of random zeros and ones
    """

    amount = rows * columns
    num_ones = amount * percentage // 100
    num_zeros = amount - num_ones
    result = []
    temp = rcf.generate_random_list(num_ones, num_zeros)
    for num in range(0, len(temp), columns):
        result.append(temp[num:num+columns])
    return result


def generate_output_file(rows, columns, density):

    """
    Function for generating a random data set file based on the given inputs
    :param rows: the number of rows in the data set
    :param columns: the number of columns in the data set
    :param density: the percentage of density of ones in the data set
    """

    if not os.path.exists("generated_files"):
        os.makedirs("generated_files")
    filename = "out" + datetime.datetime.now().strftime("%Y-%m-%d_%H-%M") + ".rcf"
    path = "generated_files/" + filename
    row_names = ' | '.join(map(str, rcf.generate_row_names(rows)))
    column_names = ' | '.join(map(str, rcf.generate_column_names(columns)))
    data_list = generate_data(int(rows), int(columns), int(density))
    with open(path, 'w') as output_file:
        output_file.write("[Relational Context]\n")
        output_file.write(filename + "\n")
        output_file.write("[Binary Relation]\n")
        output_file.write("Name_of_dataset\n")
        output_file.write(row_names + "\n")
        output_file.write(column_names + "\n")
        for list_item in data_list:
            output_file.write(' '.join(map(str, list_item)) + "\n")
        output_file.write("[END Relational Context]\n")


def usage():
    print("usage: ./gen.py <number of transactions> <number of attributes> [density(in %)]")


def main():
    if len(sys.argv) < 3:
        usage()
    else:
        rows = int(sys.argv[1])
        columns = int(sys.argv[2])
        if len(sys.argv) == 3:
            generate_output_file(rows, columns, 50)
        else:
            density = int(sys.argv[3])
            generate_output_file(rows, columns, density)


if __name__ == '__main__':
    main()

